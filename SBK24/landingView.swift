//
//  landingView.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/12/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit
class landingView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    private func setupView() {
        layer.cornerRadius = 10
    }
}
