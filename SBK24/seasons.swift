//
//  seasons.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/15/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class seasonResults: Codable {
    let Results: [seasonResult]
    init (Results: [seasonResult]) {
        self.Results = Results
    }  
}

class seasonResult: Codable {
    let IdSeason: String
    let StartDate: String
    let EndDate: String
    init (IdSeason: String, StartDate: String, EndDate: String) {
        self.IdSeason = IdSeason
        self.StartDate = StartDate
        self.EndDate = EndDate
    }
}
