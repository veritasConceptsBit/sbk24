//
//  Result.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 7/7/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class Resulten: Codable {
    let Results: [Result]
    
    init (Results: [Result]) {
        self.Results = Results
    }
}
class Result: Codable {
    let LocalDate: String
    let Home: homeTypes
    let Away: homeTypes
    let Stadium : stadiumTypes
    let HomeTeamScore: Int?
    let AwayTeamScore: Int?
    let StageName: [stageTypes]
    let BallPossession: ballPosTypes?
    let Officials: [official]
    let Attendance: String?
    let Leg: String?
    let MatchDay: String?
    init(LocalDate: String, Home: homeTypes, Away: homeTypes , Stadium: stadiumTypes, HomeTeamScore: Int?, AwayTeamScore: Int?, StageName: [stageTypes], BallPossession: ballPosTypes?, Officials: [official], Attendance: String?, Leg: String?, MatchDay: String?) {
        self.LocalDate = LocalDate
        self.Home = Home
        self.Away = Away
        self.Stadium = Stadium
        self.HomeTeamScore = HomeTeamScore
        self.AwayTeamScore = AwayTeamScore
        self.StageName = StageName
        self.BallPossession  = BallPossession
        self.Officials = Officials
        self.Attendance = Attendance
        self.Leg = Leg
        self.MatchDay = MatchDay
    }

}

class homeTypes: Codable {
    let TeamName: [TeamNameTypes]
    let IdTeam: String
    let Tactics: String?
    let Side: Int?
    init(TeamName: [TeamNameTypes], IdTeam: String, Tactics: String?, Side: Int?) {
        self.TeamName = TeamName
        self.IdTeam = IdTeam
        self.Tactics = Tactics
        self.Side = Side
    }
}
class TeamNameTypes: Codable {
    let Description: String
    init(Description: String) {
        self.Description = Description
    }
}
class stadiumTypes: Codable {
    let Name: [nameTypes]
    let CityName: [CityNameTypes]
    init(Name: [nameTypes], CityName: [CityNameTypes]) {
        self.Name = Name
        self.CityName = CityName
    }
}

class nameTypes: Codable {
    let Description: String
    init(Description: String) {
        self.Description = Description
    }
}
class CityNameTypes: Codable {
    let Description: String
    init(Description: String) {
        self.Description = Description
    }
}
class stageTypes: Codable {
    let Description: String?
    init(Description: String?) {
        self.Description = Description
    }
}
class ballPosTypes: Codable {
    let OverallHome: Int
    let OverallAway: Int
    init(OverallHome: Int, OverallAway: Int) {
        self.OverallHome = OverallHome
        self.OverallAway = OverallAway
    }
}
class official: Codable {
    let NameShort: [nameShort]
    init(NameShort: [nameShort]) {
        self.NameShort = NameShort
    }
}
class nameShort: Codable {
    let Description: String
    init(Description: String) {
        self.Description = Description
    }
}
