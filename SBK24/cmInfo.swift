//
//  cmInfo.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/24/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class cmInfo: UIViewController {
    
    // vars
    var homeTactics: String?
    var awayTactics: String?
    var stageName: String?
    var homeSide: String?
    var awaySide: String?
    var homePos: String?
    var awayPos: String?
    
    // IBs
    
    @IBOutlet weak var homeTacticsLabel: UILabel!
    
    @IBOutlet weak var awayTacticsLabel: UILabel!
    
    @IBOutlet weak var stageNameLabel: UILabel!
    
    @IBOutlet weak var homeSideLabel: UILabel!
    
    @IBOutlet weak var awaySideLabel: UILabel!
    
    
    @IBOutlet weak var homePostLabel: UILabel!
    
    
    @IBOutlet weak var awayPosLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // tactics
        homeTacticsLabel.text = homeTactics
        awayTacticsLabel.text = awayTactics
        stageNameLabel.text = stageName
        homeSideLabel.text = homeSide
        awaySideLabel.text = awaySide
        homePostLabel.text = homePos
        awayPosLabel.text = awayPos
        
    }
}
