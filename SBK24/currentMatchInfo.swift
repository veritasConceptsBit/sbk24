//
//  currentMatchInfo.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/23/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class currentMatchInfo: UIViewController {
    
    // vars
    var homeTactics: String?
    var awayTactics: String?
    var stageName: String?
    var homeSides: String?
    var awaySides: String?
    var homeBall: String?
    var awayBall: String?
    
    // IBs
    
    @IBOutlet weak var homeTacticsLabel: UILabel!
    @IBOutlet weak var awayTacticsLabel: UILabel!
    
    @IBOutlet weak var stageNameLabel: UILabel!
    
    @IBOutlet weak var homeSidesLabel: UILabel!
    
    @IBOutlet weak var awaySidesLabel: UILabel!
    
    @IBOutlet weak var homeBallLabel: UILabel!
    
    @IBOutlet weak var awayBallLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // updateView
        print(homeTactics)
        homeTacticsLabel.text = homeTactics
        awayTacticsLabel.text = awayTactics
        stageNameLabel.text = stageName
        homeSidesLabel.text = homeSides
        awaySidesLabel.text = awaySides
        homeBallLabel.text = homeBall
        awayBallLabel.text = awayBall
    }
}
