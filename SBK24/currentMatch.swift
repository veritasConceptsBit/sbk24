//
//  currentMatch.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/21/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class currentMatch: UIViewController {
    
    // Vars
    var homeNameText: String?
    var homeFlagImage: UIImage?
    var homeScoreText: String?
    var awayNameText: String?
    var awayFlagImage: UIImage?
    var awayScoreText: String?
    var cityNameText: String?
    var stadiumNameText: String?
    
    // Vars for Segments
    var homeTactics: String?
    var awayTactics: String?
    var stageName: String?
    var homeSide: String?
    var awaySide: String?
    var homePos: String?
    var awayPos: String?
    
    // Stats
    var refName: String?
    var attendNo: String?
    var leg: String?
    var mDay: String?
    
    //IBs
    
    @IBOutlet weak var homeFlag: UIImageView!
    
    @IBOutlet weak var homeLabel: UILabel!
    
    @IBOutlet weak var homeScore: UILabel!
    
    @IBOutlet weak var awayScore: UILabel!
    
    @IBOutlet weak var awayFlag: UIImageView!
    
    @IBOutlet weak var awayLabel: UILabel!
    
    @IBOutlet weak var cityName: UILabel!
    
    @IBOutlet weak var stadiumName: UILabel!
    
    
    @IBOutlet weak var segmentA: UISegmentedControl!
    
    
    @IBOutlet weak var infoView: UIView!
    
    
    @IBOutlet weak var commentView: UIView!
    
    @IBOutlet weak var statView: UIView!
    
    
    @IBOutlet weak var scrollContainer: UIScrollView!
    
    
    
    // containers
    
    
    
    // location
    var gpsPrefix = "https://www.google.com/maps/dir/"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load variables
        homeFlag.image = homeFlagImage
        homeLabel.text = homeNameText
        homeScore.text = homeScoreText
        awayFlag.image = awayFlagImage
        awayLabel.text = awayNameText
        awayScore.text = awayScoreText
        cityName.text = cityNameText
        stadiumName.text = stadiumNameText

        
        // if iPad change text Size
        if (traitCollection.horizontalSizeClass.rawValue == 2) {
            let segmentIpadFont: [AnyHashable : Any] = [NSAttributedStringKey.font : UIFont.systemFont(ofSize: 34)]
            segmentA.setTitleTextAttributes(segmentIpadFont, for: .normal)
        }
        
        // make infoView Boss
        commentView.isHidden = true
        statView.isHidden = true
    } // end viewDidLoad
    
    // gps BUTTON
    
    @IBAction func gpsBTN(_ sender: Any) {
        let gpsString = gpsPrefix+stadiumNameText!.replacingOccurrences(of: " ", with: "+")+"+Stadium/"
        let gpsURL = URL(string: gpsString)
        UIApplication.shared.open(gpsURL!)    }
    
    // Segment Control
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "toInfo") {
            let destination = segue.destination as! cmInfo
            destination.homeTactics = homeTactics
            destination.awayTactics = awayTactics
            destination.stageName = stageName
            destination.homeSide = homeSide
            destination.awaySide = awaySide
            destination.homePos = homePos
            destination.awayPos = awayPos
        }
        else if (segue.identifier == "toStats") {
            let sDestiantion = segue.destination as! cmStats
            sDestiantion.refName = refName
            sDestiantion.attendNo = attendNo
            sDestiantion.leg = leg
            sDestiantion.mDay = mDay
        }
    }
    
    @IBAction func segmentControlAction(_ sender: Any) {
        scrollContainer.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        // if info
        if (segmentA.selectedSegmentIndex == 0) {
            commentView.isHidden = true
            statView.isHidden = true
            infoView.isHidden = false
        }
        // if comment
        else if (segmentA.selectedSegmentIndex == 1) {
            infoView.isHidden = true
            statView.isHidden = true
            commentView.isHidden = false
            
        }
        else if (segmentA.selectedSegmentIndex == 2) {
            infoView.isHidden = true
            commentView.isHidden = true
            statView.isHidden = false
            
        }
        }


    
    
} // end class
