//
//  futureMatch.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/19/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class futureMatch: UIViewController {
    // destination variables
    var awayNameText: String?
    var awayFlagImage: UIImage?
    var homeNameText: String?
    var homeFlagImage: UIImage?
    var cityText: String?
    var stadText: String?
    
    // location
    var gpsPrefix = "https://www.google.com/maps/dir/"
    
    
    // link IBs
    
    @IBOutlet weak var awayFlag: UIImageView!
    
    @IBOutlet weak var awayName: UILabel!
    
    
    @IBOutlet weak var homeFlag: UIImageView!
    
    @IBOutlet weak var homeName: UILabel!
    
    @IBOutlet weak var city: UILabel!
    
    
    @IBOutlet weak var stad: UILabel!
    
    
    // viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set Navigation Controller BG to Transparent
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        // assign default value
        awayName.text = awayNameText
        awayFlag.image = awayFlagImage
        homeName.text = homeNameText
        homeFlag.image = homeFlagImage
        city.text = cityText
        stad.text = stadText
        
        // stad location
        
        
        
        
        
        
    } // end did load
    
    
    @IBAction func gpsBTN(_ sender: Any) {
        let gpsString = gpsPrefix+stadText!.replacingOccurrences(of: " ", with: "+")+"+Stadium/"
        let gpsURL = URL(string: gpsString)
        UIApplication.shared.open(gpsURL!)
    }
    
    
}
