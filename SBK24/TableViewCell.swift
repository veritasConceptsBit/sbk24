//
//  TableViewCell.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 6/29/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var teamOne: UILabel!
    
    @IBOutlet weak var teamOneFlag: UIImageView!
    
    @IBOutlet weak var teamTwoFlag: UIImageView!
    
    @IBOutlet weak var teamTwo: UILabel!
    
    @IBOutlet weak var groupLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
