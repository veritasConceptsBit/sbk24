//
//  tournamentLanding.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/15/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class tournamentLanding: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITableViewDelegate, UITableViewDataSource {

    // connect Picker
    
    @IBOutlet weak var seasonPicker: UIPickerView!
    
    // connect Table
    
    @IBOutlet weak var tournamentTable: UITableView!
    
    // define picker Data
    var pickerData: [String] = [String]()
    // define picker Display
    var pickerDisplay: [String] = [String]()
    // set Title
    var viewTitle: String = ""
    // define season ID & competition ID
    var seasonID: String = ""
    var competitionID: String = ""
    // define tableResults
    private var Results = [Result]()
    final let resultPrefix: String = "https://api.fifa.com/api/v1/calendar/matches?idseason="
    final let resultMiddix: String = "&idcompetition="
    final let resultSuffix: String = "&language=ar-SA&count=1000"
    var resultString: String = ""
    var resultURL: URL = URL(string: "https://api.fifa.com/api/v1/calendar/matches?idseason=")!
    
    // date operations
    let todaysDate = Date()
    let inputFormatter = DateFormatter()
    let outputFormatter = DateFormatter()
    var isItFuture: Bool = false
    
    // Stats
    var nullCity: String = " "
    var nullStad: String = " "
    

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set Navigation Controller BG to Transparent
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        // Set View Title
        self.title = viewTitle
        // Connect Picker source and delegate
        self.seasonPicker.dataSource = self
        self.seasonPicker.delegate = self
        // set Picker selection to row 0
        seasonPicker.selectRow(0, inComponent: 0, animated: true)
        // start tableDate function on row 0 data
        seasonID = pickerData[0]
        buildTable()
        
    } // end view did load
    
    // conform to protocol
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerDisplay.count
    }
    
    // customize picker cell
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        return NSAttributedString(string: pickerDisplay[row], attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
    }
    
    
    // change seasonID based on picker selection
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        seasonID = pickerData[row]
        print(pickerData[row])
        buildTable()
    }
    
    // Build Table
    func buildTable() {
        resultString = resultPrefix+seasonID+resultMiddix+competitionID+resultSuffix
        resultURL = URL(string: resultString)!
        let downloadResultURL = resultURL
        // Start URL Task
        URLSession.shared.dataTask(with: downloadResultURL) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("Tournament Data Download Failed")
                return
            }
            print("Tournament Download Success")
            do {
                let decoder = JSONDecoder()
                let tournamentResults = try decoder.decode(Resulten.self, from: data)
                self.Results = tournamentResults.Results
                DispatchQueue.main.async {
                    self.tournamentTable.reloadData()
                    // Scroll Table to Top
                    let rowIndex:IndexPath = NSIndexPath(row: 0, section: 0) as IndexPath
                    self.tournamentTable.scrollToRow(at: rowIndex, at: .top, animated: false)
                }
            }
            catch {
                print(error)
            }
        }.resume()
       
    }
    
    
    // uiTableView conformation
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Results.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tournamentTable.dequeueReusableCell(withIdentifier: "GameCell") as? GameCell else { return UITableViewCell()}
        // Date Label
        let dateString = Results[indexPath.row].LocalDate
        let datePosT = dateString.index(of: "T")!
        let dateUntilT = dateString[..<datePosT]
        let finalDate = String(dateUntilT)
        cell.dateLabel.text = finalDate
        
        // isFutureBool
        inputFormatter.dateFormat = "yyyy-MM-dd"
        outputFormatter.dateFormat = "yyyy-MM-dd"
        let todaysFinalString = inputFormatter.string(from: todaysDate)
        let matchDate = outputFormatter.date(from: finalDate)
        let todaysFinalDate = outputFormatter.date(from: todaysFinalString)
        cell.isFuture = (matchDate!>todaysFinalDate!)
        
        // match Stats
        // cityName
        if(!Results[indexPath.row].Stadium.CityName.isEmpty) {
            nullCity = Results[indexPath.row].Stadium.CityName[0].Description
        }
        else {
            nullCity = " "
        }
        if(!nullCity.isEmpty) {
            cell.city = nullCity
        }
        else {
            cell.city = " "
        }
        // Stadium
        if(!Results[indexPath.row].Stadium.Name.isEmpty) {
            nullStad = Results[indexPath.row].Stadium.Name[0].Description
        }
        else {
            nullStad = " "
        }
        if(!nullStad.isEmpty) {
            cell.stadium = nullStad
        }
        else {
            cell.stadium = " "
        }
        
        // currents
        if (Results[indexPath.row].HomeTeamScore != nil ) {
            cell.homeScore = String(Results[indexPath.row].HomeTeamScore!)
        }
        
        if (Results[indexPath.row].AwayTeamScore != nil) {
            cell.awayScore = String(Results[indexPath.row].AwayTeamScore!)
        }
        
        // currents subviews
        // tactics
        if (Results[indexPath.row].Home.Tactics != nil) {
            cell.homeTactics = Results[indexPath.row].Home.Tactics!
        }
        if (Results[indexPath.row].Away.Tactics != nil) {
            cell.awayTactics = Results[indexPath.row].Away.Tactics!
        }
        // stage
        if (!Results[indexPath.row].StageName.isEmpty) {
            cell.stageName = Results[indexPath.row].StageName[0].Description!
        }
        // sides
        if (Results[indexPath.row].Home.Side != nil) {
            cell.homeSide = String(Results[indexPath.row].Home.Side!)
        }
        if (Results[indexPath.row].Away.Side != nil) {
            cell.awaySide = String(Results[indexPath.row].Away.Side!)
        }
        // Ball Pos
        if (Results[indexPath.row].BallPossession != nil) {
            cell.homePos = String(Results[indexPath.row].BallPossession!.OverallHome)
            cell.homePos += "%"
            cell.awayPos = String(Results[indexPath.row].BallPossession!.OverallAway)
            cell.awayPos += "%"
        }
        // Stats
        // Ref
        if (!Results[indexPath.row].Officials.isEmpty) {
            cell.refName = Results[indexPath.row].Officials[0].NameShort[0].Description
        }
        // Attendance
        if (Results[indexPath.row].Attendance != nil) {
            cell.attendNo = Results[indexPath.row].Attendance!
        }
        // Leg
        if (Results[indexPath.row].Leg != nil) {
            cell.Leg = Results[indexPath.row].Leg!
        }
        // MatchDay
        if (Results[indexPath.row].MatchDay != nil) {
            cell.mDay = Results[indexPath.row].MatchDay!
        }
        

        
        // Home Label
        let homeTeamName  = Results[indexPath.row].Home.TeamName[0].Description
        cell.homeTeamName.text = homeTeamName
        
        // Home Image
        let rawHomeFlag = Results[indexPath.row].Home.IdTeam
        var finalHomeFlag = "https://img.fifa.com/mm/teams/"
        finalHomeFlag += rawHomeFlag+"/"+rawHomeFlag+"x5.png"
        if let homeFlagURL = URL(string: finalHomeFlag) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf:homeFlagURL)
                if let data = data {
                    let homeFlag = UIImage(data: data)
                    DispatchQueue.main.async {
                        cell.homeFlag.image = homeFlag
                    }
                }
                
            }
        }
        
        // Away Image
        let rawAwayFlag = Results[indexPath.row].Away.IdTeam
        var finalAwayFlag = "https://img.fifa.com/mm/teams/"
        finalAwayFlag += rawAwayFlag+"/"+rawAwayFlag+"x5.png"
        if let awayFlagURL = URL(string: finalAwayFlag) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf:awayFlagURL)
                if let data = data {
                    let awayFlag = UIImage(data: data)
                    DispatchQueue.main.async {
                        cell.awayFlag.image = awayFlag
                    }
                }
                
            }
        }
        
        
        // Away Label
        let AwayTeamName = Results[indexPath.row].Away.TeamName[0].Description
        cell.awayTeamName.text = AwayTeamName
        
        
        // Competition Label
        /*let groupDesc = Results[indexPath.row].CompetitionName[0].Description
        cell.groupLabel.text = groupDesc*/
        
        
        //return Cell
        return cell
    } // end cell buildup
    
    // OnCellSelect
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // set variables
        let selectedPath = tournamentTable.indexPathForSelectedRow
        let selectedCell = tournamentTable.cellForRow(at: selectedPath!) as! GameCell
        let pAwayFlag = selectedCell.awayFlag.image
        let pAwayName = selectedCell.awayTeamName.text
        let pHomeFlag = selectedCell.homeFlag.image
        let pHomeName = selectedCell.homeTeamName.text
        let pCity = selectedCell.city
        let pStad = selectedCell.stadium
        let phomeScore = selectedCell.homeScore
        let pawayScore = selectedCell.awayScore
        isItFuture = selectedCell.isFuture
        let phomeTactics = selectedCell.homeTactics
        let pawayTactics = selectedCell.awayTactics
        let pstageName = selectedCell.stageName
        let phomeSide = selectedCell.homeSide
        let pawaySide = selectedCell.awaySide
        let phomeBall = selectedCell.homePos
        let pawayBall = selectedCell.awayPos
        let prefName = selectedCell.refName
        let pattendNo = selectedCell.attendNo
        let pLeg = selectedCell.Leg
        let pmDay = selectedCell.mDay
        // is it a future match?
        if(isItFuture) {
            // define destination
            let sDestination = segue.destination as! futureMatch
            sDestination.awayFlagImage = pAwayFlag
            sDestination.awayNameText = pAwayName
            sDestination.homeFlagImage = pHomeFlag
            sDestination.homeNameText = pHomeName
            sDestination.cityText = pCity
            sDestination.stadText = pStad
        }
            // else if current
        else {
            // define destination main
            let cDestination = segue.destination as! currentMatch
            cDestination.homeNameText = pHomeName
            cDestination.homeFlagImage = pHomeFlag
            cDestination.awayNameText = pAwayName
            cDestination.awayFlagImage = pAwayFlag
            cDestination.homeScoreText = phomeScore
            cDestination.awayScoreText = pawayScore
            cDestination.cityNameText = pCity
            cDestination.stadiumNameText = pStad
            cDestination.homeTactics = phomeTactics
            cDestination.awayTactics = pawayTactics
            cDestination.stageName = pstageName
            cDestination.homeSide = phomeSide
            cDestination.awaySide = pawaySide
            cDestination.homePos = phomeBall
            cDestination.awayPos = pawayBall
            cDestination.refName = prefName
            cDestination.attendNo = pattendNo
            cDestination.leg = pLeg
            cDestination.mDay = pmDay
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // is it a future match?
        let selectedPath = tournamentTable.indexPathForSelectedRow
        let selectedCell = tournamentTable.cellForRow(at: selectedPath!) as! GameCell
        isItFuture = selectedCell.isFuture
        if(isItFuture) {
            performSegue(withIdentifier: "toFutureMatch", sender: self)
        }
            // if current match
        else {
            performSegue(withIdentifier: "toCurrentMatch", sender: self)
        }
        
    }
    
    
    
    
}
