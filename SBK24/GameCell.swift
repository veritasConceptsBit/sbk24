//
//  GameCell.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 7/7/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class GameCell: UITableViewCell {

    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var homeFlag: UIImageView!
    @IBOutlet var homeTeamName: UILabel!
    @IBOutlet var awayFlag: UIImageView!
    @IBOutlet var awayTeamName: UILabel!
    var isFuture:Bool = false
    var city: String = " "
    var stadium: String = " "
    var homeScore: String = "?"
    var awayScore: String = "?"
    var homeTactics: String = "لايوجد"
    var awayTactics: String = "لايوجد"
    var stageName: String = "لاتوجد معلومات"
    var homeSide: String = "0"
    var awaySide: String = "0"
    var homePos: String = "لايوجد"
    var awayPos: String = "لايوجد"
    var refName: String = "لاتوجد معلومات"
    var attendNo: String = "لاتوجد معلومات"
    var Leg: String = "لاتوجد معلومات"
    var mDay: String = "لاتوجد معلومات"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
