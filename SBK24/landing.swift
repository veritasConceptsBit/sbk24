//
//  landing.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/12/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class landing: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Exit if no internet
        // Check if Internet is On
        if(!Reachability.isInternetAvailable()) {
            // Alert
            // Start Alert
            let alert = UIAlertController(title: "Error خطأ", message: "This application requires an internet connection\nهذا التطبيق يتطلب أن يكون لديك اتصال انترنت", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Exit خروج", style: .default, handler: { action in
                switch action.style{
                case .default:
                    exit(0);
                    
                case .cancel:
                    print("cancel")
                    
                case .destructive:
                    print("destructive")
                    
                    
                }}))
            self.present(alert, animated: true, completion: nil)
            // End Alert
        }


        // Set Navigation Controller BG to Transparent
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    } // end did load
    var destinationURL = ""
    // Prepare for Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toWebView" {
            let destination = segue.destination as! webView
            destination.browserString = destinationURL
        }
    }
    @IBAction func tournamentsBTN(_ sender: Any) {
        performSegue(withIdentifier: "toTournaments", sender: self)
    }
    
    @IBAction func myVacayBTN(_ sender: Any) {
        destinationURL = "http://www.sbk24.com/index.php?route=product/category&path=59"
        performSegue(withIdentifier: "toWebView", sender: self)
    }
    
    @IBAction func myPlaygroundBTN(_ sender: Any) {
        destinationURL = "http://www.sbk24.com/index.php?route=product/category&path=62"
        performSegue(withIdentifier: "toWebView", sender: self)
    }
    
    
    @IBAction func sbk24BTN(_ sender: Any) {
        destinationURL = "http://www.sbk24.com/"
        performSegue(withIdentifier: "toWebView", sender: self)
    }
    
    
    @IBAction func codeBaseBtn(_ sender: Any) {
        let veritasURL = URL(string: "http://codebase.veritas-concepts.com")
        UIApplication.shared.open(veritasURL!)
    }
    
} // end class
