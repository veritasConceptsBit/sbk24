//
//  cmStats.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/24/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class cmStats: UIViewController {
    // vars
    var refName: String?
    var attendNo: String?
    var leg: String?
    var mDay: String?
    
    // IBs
    
    @IBOutlet weak var refLabel: UILabel!
    
    
    @IBOutlet weak var attendLabel: UILabel!
    
    
    @IBOutlet weak var legLabel: UILabel!
    
    
    @IBOutlet weak var mDayLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load vars
        refLabel.text = refName
        attendLabel.text = attendNo
        legLabel.text = leg
        mDayLabel.text = mDay
    }
}
