//
//  webView.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/12/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit
class webView: UIViewController {
    
    @IBOutlet weak var browserView: UIWebView!
    var browserString: String = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set Navigation Controller BG to Transparent
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        // Browse
        let browserURL = URL (string: browserString)
        let requestObj = URLRequest(url: browserURL!)
        browserView.loadRequest(requestObj)
    }
}
