//
//  tournaments.swift
//  SBK24
//
//  Created by Ali Almir Mahmoud on 8/12/18.
//  Copyright © 2018 Veritas Concepts. All rights reserved.
//

import UIKit

class tournaments: UIViewController {
    // define destination variables
    var destinationTitle: String = ""
    var destinationCompetitionID: String = ""
    // destination value arrays [IdSeason]
    var tournamentArray = [String]()
    var tournamentArrayUAE = [String]()
    var tournamentArraySaudi = [String]()
    var tournamentArrayUEFA = [String]()
    // destination display arrays [StartDate-EndDate]
    var tournamentDateArray = [String]()
    var tournamentDateArrayUAE = [String]()
    var tournamentDateArrayKSA = [String]()
    var tournamentDateArrayUEFA = [String]()
    
    // Sort out competition Strings
    var tournamentUAE: String = "2000000090"
    var tournamentSaudi: String = "2000000091"
    var tournamentUEFA: String = "2000001032"
    var seasonGetter: String = "https://api.fifa.com/api/v1/seasons?idCompetition="
    var seasonSuffix: String = "&language=ar"
    
    // Results Arrays
    var uaeResults = [seasonResult]()
    var ksaResults = [seasonResult]()
    var uefaResults = [seasonResult]()
    
    func generateSeasons() {
        // genrate individual season Strings
        let seasonUAE = seasonGetter+tournamentUAE+seasonSuffix
        let seasonKSA = seasonGetter+tournamentSaudi+seasonSuffix
        let seasonUEFA = seasonGetter+tournamentUEFA+seasonSuffix
        // generate URLs
        let seasonUAEURL = URL(string: seasonUAE)
        let seasonKSAURL = URL(string: seasonKSA)
        let seasonUEFAURL = URL(string: seasonUEFA)
        // generate uae season
        URLSession.shared.dataTask(with: seasonUAEURL!) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("UAE download Failed")
                return
            }
            print("UAE download success")
            do {
                // set UAE Season ID
                let uaeDecoder = JSONDecoder()
                let downloadedUAEResults = try uaeDecoder.decode(seasonResults.self, from:data)
                self.uaeResults = downloadedUAEResults.Results
                for i in 0..<self.uaeResults.count {
                    self.tournamentArrayUAE.append(self.uaeResults[i].IdSeason)
                    let startDate = self.uaeResults[i].StartDate
                    let posT = startDate.index(of: "T")!
                    let tillT = startDate[..<posT]
                    let startFinalDate = String(tillT)
                    let endDate = self.uaeResults[i].EndDate
                    let posTEnd = endDate.index(of: "T")!
                    let tillTEnd = endDate[..<posTEnd]
                    let endFinalDate = String(tillTEnd)
                    let finalDate = endFinalDate+" - "+startFinalDate
                    self.tournamentDateArrayUAE.append(finalDate)
                }
            } catch {
               print(error)
            }
        }.resume()
        
        // generate ksa season
        URLSession.shared.dataTask(with: seasonKSAURL!) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("Saudi download Failed")
                return
            }
            print("Saudi download success")
            do {
                // set Saudi Season ID
                let saudiDecoder = JSONDecoder()
                let downloadedKSAResults = try saudiDecoder.decode(seasonResults.self, from:data)
                self.ksaResults = downloadedKSAResults.Results
                for i in 0..<self.ksaResults.count {
                    self.tournamentArraySaudi.append(self.ksaResults[i].IdSeason)
                    let startDate = self.ksaResults[i].StartDate
                    let posT = startDate.index(of: "T")!
                    let tillT = startDate[..<posT]
                    let startFinalDate = String(tillT)
                    let endDate = self.ksaResults[i].EndDate
                    let posTEnd = endDate.index(of: "T")!
                    let tillTEnd = endDate[..<posTEnd]
                    let endFinalDate = String(tillTEnd)
                    let finalDate = endFinalDate+" - "+startFinalDate
                    self.tournamentDateArrayKSA.append(finalDate)
                }
            } catch {
                print(error)
            }
            }.resume()
        
        // generate uefa season
        URLSession.shared.dataTask(with: seasonUEFAURL!) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("UEFA download Failed")
                return
            }
            print("UEFA download success")
            do {
                // set UEFA Season ID
                let uefaDecoder = JSONDecoder()
                let downloadeduefaResults = try uefaDecoder.decode(seasonResults.self, from:data)
                self.uefaResults = downloadeduefaResults.Results
                for i in 0..<self.uefaResults.count {
                    self.tournamentArrayUEFA.append(self.uefaResults[i].IdSeason)
                    let startDate = self.uefaResults[i].StartDate
                    let posT = startDate.index(of: "T")!
                    let tillT = startDate[..<posT]
                    let startFinalDate = String(tillT)
                    let endDate = self.uefaResults[i].EndDate
                    let posTEnd = endDate.index(of: "T")!
                    let tillTEnd = endDate[..<posTEnd]
                    let endFinalDate = String(tillTEnd)
                    let finalDate = endFinalDate+" - "+startFinalDate
                    self.tournamentDateArrayUEFA.append(finalDate)
                }
            } catch {
                print(error)
            }
            }.resume()
        } // end generateSeasons
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set Navigation Controller BG to Transparent
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        // generate URLs
        generateSeasons()
    }
    
    
    
    // Prepare for Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! tournamentLanding
        destination.viewTitle = destinationTitle
        destination.competitionID = destinationCompetitionID
        destination.pickerData = tournamentArray
        destination.pickerDisplay = tournamentDateArray
    }
    
    // Setup Buttons
    
    @IBAction func uaeBTN(_ sender: Any) {
        destinationTitle = "الدوري الإماراتي"
        destinationCompetitionID = tournamentUAE
        tournamentArray = tournamentArrayUAE
        tournamentDateArray = tournamentDateArrayUAE
        performSegue(withIdentifier: "toTournamentLanding", sender: self)
    }
    
    
    @IBAction func ksaBTN(_ sender: Any) {
        destinationTitle = "الدوري السعودي"
        destinationCompetitionID = tournamentSaudi
        tournamentArray = tournamentArraySaudi
        tournamentDateArray = tournamentDateArrayKSA
        performSegue(withIdentifier: "toTournamentLanding", sender: self)
    }
    
    
    @IBAction func euBTN(_ sender: Any) {
        destinationTitle = "الدوري الأوروبي"
        destinationCompetitionID = tournamentUEFA
        tournamentArray = tournamentArrayUEFA
        tournamentDateArray = tournamentDateArrayUEFA
        performSegue(withIdentifier: "toTournamentLanding", sender: self)
    }
}

